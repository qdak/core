<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Boxes\Items;

/**
 * Class Plugin
 *
 * @package JTL\Boxes\Items
 */
class Plugin extends AbstractBox
{

}
