<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Mail\Template;

/**
 * Class NewCustomerRegistration
 * @package JTL\Mail\Template
 */
class NewCustomerRegistration extends CustomerAccountDeleted
{
    protected $id = \MAILTEMPLATE_NEUKUNDENREGISTRIERUNG;
}
