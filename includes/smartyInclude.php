<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */
$smarty = \JTL\Smarty\JTLSmarty::getInstance();

return $smarty;
