{**
 * @copyright (c) JTL-Software-GmbH
 * @license https://jtl-url.de/jtlshoplicense
 *}
{block name='poll-result'}
    {block name='poll-result-heading'}
        {opcMountPoint id='opc_before_heading'}
        <h1>{lang key='umfrage' section='umfrage'}</h1>
    {/block}
    {block name='poll-result-content'}

    {/block}
{/block}
