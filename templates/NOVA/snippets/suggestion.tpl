{**
 * @copyright (c) JTL-Software-GmbH
 * @license https://jtl-url.de/jtlshoplicense
 *}
{block name='snippets-suggestion'}
    <div>{$result->keyword} <span class="badge badge-primary float-right">{$result->quantity}</span></div>
{/block}
