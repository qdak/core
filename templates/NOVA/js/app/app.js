/* global */

import './globals.js'
import './slider.js'
import './gallery.js'

/* plugins */

import './plugins/navscrollbar.js'
import './plugins/tabdrop.js'

/* view specifics */

import './views/header.js'
import './views/productdetails.js'
