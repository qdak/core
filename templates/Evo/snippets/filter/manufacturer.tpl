{**
 * @copyright (c) JTL-Software-GmbH
 * @license https://jtl-url.de/jtlshoplicense
 *}
<ul class="{if isset($class)}{$class}{else}nav nav-list{/if}">
    {foreach $Suchergebnisse->Herstellerauswahl as $Hersteller}
        {if $Hersteller->nAnzahl >= 1}
            <li>
                <a rel="nofollow" href="{$Hersteller->cURL}">
                    <span class="badge pull-right">{if !isset($nMaxAnzahlArtikel) || !$nMaxAnzahlArtikel}{$Hersteller->nAnzahl}{/if}</span>
                    <span class="value">
                        <i class="fa {if $NaviFilter->hasManufacturerFilter() && $NaviFilter->getManufacturerFilter()->getValue() == $Hersteller->kHersteller}fa-check-square-o{else}fa-square-o{/if} text-muted"></i>
                        {$Hersteller->cName|escape:'html'}
                    </span>
                </a>
            </li>
        {/if}
    {/foreach}
</ul>
