<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */
require_once __DIR__ . '/includes/admininclude.php';

$oAccount->permission('SETTINGS_META_KEYWORD_BLACKLIST_VIEW', true, true);

require_once __DIR__ . '/globalemetaangaben.php';
