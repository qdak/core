<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */

define('PFAD_ROOT', dirname(__DIR__) . '/');
require_once __DIR__ . '/../includes/defines.php';
require_once __DIR__ . '/../includes/autoload.php';
