Plugins
=======

.. toctree::
    :hidden:

    allgemein
    aufbau
    bootstrapping
    variablen
    hooks
    hook_list
    container
    cache
    mailing
    fehlercodes
    sicherheit
    short_checkout
    hinweise

.. include:: /shop_plugins/map.rst.inc
