HOOK_SITEMAP_EXPORT_GENERATED (165)
===================================

Triggerpunkt
""""""""""""

Nach dem Erzeugen der Sitemap

Parameter
"""""""""

``array`` **nAnzahlURL_arr**
    **nAnzahlURL_arr** Anzahl der erzeugten URLs

``mixed`` **fTotalZeit**
    **fTotalZeit** verstrichene Zeit (Report)
