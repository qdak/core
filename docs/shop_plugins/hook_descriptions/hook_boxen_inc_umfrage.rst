HOOK_BOXEN_INC_UMFRAGE (95)
===========================

Triggerpunkt
""""""""""""

Vor der Anzeige der Umfrage

Parameter
"""""""""

``JTL\Boxes\Items\Poll`` **&box**
    **&box** Umfrage-Objekt

``array`` **&cache_tags**
    **&cache_tags** Umfang des Zwischenspeicherns

``bool`` **cached**
    **cached** Flag "wurde vom Zwischenspeicher gelesen?"
