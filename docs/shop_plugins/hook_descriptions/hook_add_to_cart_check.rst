HOOK_ADD_TO_CART_CHECK (261)
============================

Triggerpunkt
""""""""""""

Am Ende der Prüfung "zum Warenkorb hinzufügen"

Parameter
"""""""""

``JTL\Catalog\Product\Artikel`` **product**
    **product** Artikelobjekt

``int`` **quantity**
    **quantity** Anzahl

``array`` **attributes**
    **attributes** Artikelattribute

``int`` **accuracy**
    **accuracy** Stellenanzahl des Preise (Berechnung)

``array`` **&redirectParam**
    **&redirectParam** Redirect-Parameter
