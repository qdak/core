JTL-Shop nutzt die Templateengine Smarty 3 (http://www.smarty.org). Im Frontend kommen HTML5, Bootstrap 3 (http://getbootstrap.com/) sowie jQuery (https://jquery.com/) + diverse jQuery-Plugins zum Einsatz.

Es stehen verschiedene Möglichkeiten für individuelle Templateanpassungen zur Verfügung, auf die in dieser Dokumentation eingegangen wird.

:doc:`/shop_templates/evo`
  Hier finden Sie Informationen über das neue JTL-Shop-Template Evo.

:doc:`/shop_templates/structure`
  Hier finden Sie Informationen zur Ordner- und Dateistruktur sowie zum Aufbau der template.xml.

:doc:`/shop_templates/template_settings`
  Die einzelnen Einstellungen des Evo-Templates genauer erklärt.

:doc:`/shop_templates/eigenes_template`
  Child-Templates sind der bevorzugte Weg zum eigenen Template.

:doc:`/shop_templates/eigenes_theme`
  Das individuelle Design des Shops wird über Stylesheets (CSS) in Themes definiert.

:doc:`/shop_templates/blocks_list`
  Eine Liste mit allen Blocks im EVO-Template, zum Überschreiben oder Erweitern in einem Child-Template.

:doc:`/shop_templates/tipps_tricks`
  Template How-To's und Best Practises. Tipps und Tricks für Ihr Template.

:doc:`/shop_templates/livestyler`
  Wenn Sie nur die Farben und Abstände im Evo-Template ändern wollen, ist der Evo-LiveStyler das richtige Werkzeug für Sie.

:doc:`/shop_templates/debug`
  Nutzen Sie das, in JTL-Shop integrierte, Plugin JTL Debug, um Template- und Shop-Informationen zu untersuchen.

:doc:`/shop_templates/short_checkout`
  Welche Änderungen gibt es im Zusammenhang mit dem verkürzten Checkout ab Version 4.06.

:doc:`/shop_templates/overlays`
  Wie können Overlays (z.B. für "Bestseller") angelegt werden.
