Evo-LiveStyler
==============

.. contents::
    Inhalt

****************************
 Was ist der Evo-LiveStyler?
****************************

Mit dem Evo-LiveStyler ist es ganz einfach, Änderungen an Variablen, Farben, Schriften, Abständen etc. live anzupassen.
Änderungen die hier vorgenommen werden, können nicht durch ein Wechseln des Themes rückgängig gemacht werden.

Voraussetzung
_____________

Um den **Evo-LiveStyler** verwenden zu können, muss zunächst das Plugin **Evo-Editor** installiert und aktiviert werden.
Beide Plugins werden standardmäßig mit JTL-Shop ausgeliefert.

.. note::

    **Achtung:** Der **Evo-LiveStyler** kann nur verwendet werden, wenn Sie als Admin eingeloggt sind und sowohl das Back- als auch Frontend über dasselbe Protokoll (also jeweils http:// oder https://) aufgerufen werden!

Installation
____________

Den **Evo-Editor** sowie den **Evo-LiveStyler** installieren Sie im Shop-Backend unter ``Plugins -> Pluginverwaltung -> Verfügbar``.
Hier wählen Sie das zu installierende Plugin aus und klicken unten auf installieren.

.. note::

    **Achtung:** Der **Evo-LiveStyler** ist nicht für den Einsatz in einer Produktiv-Umgebung gedacht!

Nach Aktivierung des **Evo-Editor** kann nun in den Template-Einstellungen ``Templates -> JTL-Shop-Evo -> Einstellungen`` der **Evo-LiveStyler** aktiviert werden.

Um nun Änderungen am Theme vornehmen zu können, gehen Sie bitte in das Frontend Ihres Shops. Sie sehen nun oben links einen Button mit dem Titel ``LiveStyler öffnen``.

.. image:: /_images/livestyler.jpg

Der **Evo-LiveStyler** zeigt Ihnen alle Variablen und Ihre Werte. Diese Variablen verändern das Aussehen Ihres Shops bezüglich Farben, Abstände und Schriftgrößen.
Alle verfügbaren Variablen finden Sie auf dieser `Seite <http://getbootstrap.com/customize/#colors>`_.

Wenn Sie die gewünschten Änderungen vorgenommen haben, können Sie unten auf den Button ``Vorschau`` klicken, um zu sehen, wie Ihre Änderungen wirken. Sind Sie zufrieden, klicken Sie auf den Button ``Speichern``.
