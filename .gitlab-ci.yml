variables:
  GLOBAL_CI_REGISTRY_URL: "registry.gitlab.com/jtl-software/jtl-shop/core"

stages:
  - codeQuality
  - cleanUpCodeQuality
  - test
  - cleanUpTest
  - build
  - deploy
  - cleanUp
  - tagsDeployment
  - stagingPre
  - staging
  - stagingMarketplaceDev

.codeQualityScripts: &codeQualityScripts |
  function codeQualityCheck() {
    export CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-code-quality-$CI_COMMIT_SHA

    echo "Logging to GitLab Container Registry with CI credentials..."
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    echo ""
    echo "Code quality check of projekt in docker image codesniffer..."

    docker run \
      -v $(pwd):/code \
      -w /code \
      --name="$CI_CONTAINER_NAME" \
      $GLOBAL_CI_REGISTRY_URL/codesniffer \
      sh -c "bash build/scripts/global-codesniffer.sh /code"

    docker stop "$CI_CONTAINER_NAME"
    docker rm -fv "$CI_CONTAINER_NAME" >/dev/null

    echo "Code quality check finished"
  }

.testingScript: &testingScript |
  export CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-testing-$CI_COMMIT_SHA-php$PHP_VERSION_TEST_STEP

  function testing() {
    echo "Logging to GitLab Container Registry with CI credentials..."
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    echo ""
    echo "Testing projekt in PHP $PHP_VERSION_TEST_STEP docker image..."

    docker run \
      -w '/creation' \
      --volume $(pwd):/dockerBuild \
      --name="$CI_CONTAINER_NAME" \
      $GLOBAL_CI_REGISTRY_URL/testing-php$PHP_VERSION_TEST_STEP \
      sh -c "cp -r /dockerBuild/. /creation; bash build/scripts/tests.sh"

    docker stop "$CI_CONTAINER_NAME"
    docker rm -fv "$CI_CONTAINER_NAME" >/dev/null

    echo "Tests finished"
  }

.cleanUpTesting: &cleanUpTesting |
  function failedCodeQuality() {
    CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-code-quality-$CI_COMMIT_SHA
    running=$(docker inspect -f '{{.State.Running}}' $CI_CONTAINER_NAME)

    if [[ ! -z "$running" ]]; then
      docker stop $CI_CONTAINER_NAME && docker rm -fv $CI_CONTAINER_NAME
    fi
  }
  function failedTesting() {
    CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-testing-$CI_COMMIT_SHA-php$PHP_VERSION_TEST_STEP
    running=$(docker inspect -f '{{.State.Running}}' $CI_CONTAINER_NAME)

    if [[ ! -z "$running" ]]; then
      docker stop $CI_CONTAINER_NAME && docker rm -fv $CI_CONTAINER_NAME
    fi
  }

.preBuildDeployScripts: &preBuildDeployScripts |
  export CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-build-deploy-$CI_COMMIT_SHA
  export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
  export BUILD_SCRIPT_PATH=build/scripts/build.sh
  export DEPLOY_SCRIPT_PATH=build/scripts/deploy.sh

  function dockerLogin() {
    echo "Logging to GitLab Container Registry with CI credentials..."
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    echo ""
  }
  function buildProcess() {
    RAND_CHAR=$(date +%s|sha256sum|head -c 6)
    DB_NAME="build_$RAND_CHAR"
    DB_HOST_NET=$(docker network inspect -f '{{range .Containers}}{{if eq .Name "mariadb-10-2-container"}}{{.IPv4Address}}{{end}}{{end}}' mysql-php-network)
    DB_HOST="${DB_HOST_NET::-3}"

    echo "Building projekt in PHP 7.1 and mariadb server 10.2 docker image..."
    docker run \
      -d -it \
      -w '/creation' \
      --volume $CI_PROJECT_DIR.tmp/CI_SERVER_TLS_CA_FILE:$CI_PROJECT_DIR.tmp/CI_SERVER_TLS_CA_FILE \
      --volume $(pwd):/dockerBuild \
      --name="$CI_CONTAINER_NAME" \
      --network mysql-php-network \
      $GLOBAL_CI_REGISTRY_URL/build-deploy \
      bash

    docker exec \
      -w '/creation' \
      $CI_CONTAINER_NAME \
      sh -c "cp -r /dockerBuild/. /creation; bash $BUILD_SCRIPT_PATH '/creation' $CI_COMMIT_REF_NAME $CI_COMMIT_SHA '$DB_HOST' 'root' '$DOCKER_MARIADB_PASSWORD' 'build_$DB_NAME'"

    echo "Build finished"
  }
  function deployProcess() {
    echo "Deploying archive to build server..."
    docker start $CI_CONTAINER_NAME

    docker exec \
      -w '/creation' \
      $CI_CONTAINER_NAME \
      /bin/bash -c "bash $DEPLOY_SCRIPT_PATH $CI_PROJECT_NAME $CI_COMMIT_REF_NAME /archive"

    docker cp $CI_CONTAINER_NAME:/archive/. $CI_BUILD_SERVER_ARCHIVE_PATH
    docker network disconnect mysql-php-network $CI_CONTAINER_NAME
    docker stop "$CI_CONTAINER_NAME"
    docker rm -fv "$CI_CONTAINER_NAME" >/dev/null
  }

.postBuildDeployScripts: &postBuildDeployScripts |
  function dockerLogout() {
    echo "Logout from GitLab Container Registry with CI credentials..."
    docker logout "$CI_REGISTRY"
    echo ""
  }

.cleanUpScript: &cleanUpScript |
  function failedScript() {
    CI_CONTAINER_NAME=ci-job-$CI_PROJECT_NAME-build-deploy-$CI_COMMIT_SHA
    running=$(docker inspect -f '{{.State.Running}}' $CI_CONTAINER_NAME)

    if [[ ! -z "$running" ]]; then
      docker network disconnect mysql-php-network $CI_CONTAINER_NAME
      docker stop $CI_CONTAINER_NAME && docker rm -fv $CI_CONTAINER_NAME
    fi
  }

.devCheckoutScripts: &devCheckoutScripts |
  export FILENAME="${CI_COMMIT_REF_NAME//[\/\.]/-}";
  export EXTRACTION_PATH="$RUNNER_PATH/shop-$FILENAME";

  function downloadProcess() {
    RUNNER_PATH_ZIP="$EXTRACTION_PATH.zip";
    DOWNLOAD_URL="https://build.jtl-shop.de/get/shop-$FILENAME.zip";

    curl -o $RUNNER_PATH_ZIP $DOWNLOAD_URL
    unzip -qo $RUNNER_PATH_ZIP -d $EXTRACTION_PATH
    rm $RUNNER_PATH_ZIP
  }
  function deployProcess() {
    PHP_VERSION=$1;
    DEPLOY_PATH="$STAGING_USER_PATH/php$PHP_VERSION/$FILENAME";
    MIGRATION_COUNT_BEFORE=$(ls -1 $DEPLOY_PATH/update/migrations/ | wc -l);

    sudo /usr/bin/rsync -rt -og --chown=www-data:www-data $EXTRACTION_PATH/. $DEPLOY_PATH;

    MIGRATION_COUNT_AFTER=$(ls -1 $DEPLOY_PATH/update/migrations/ | wc -l);

    if [[ $MIGRATION_COUNT_AFTER -gt $MIGRATION_COUNT_BEFORE ]]; then
      php -r "
      require_once '$DEPLOY_PATH/includes/globalinclude.php'; \
      \$time    = date('YmdHis'); \
      \$manager = new MigrationManager(Shop::Container()->getDB()); \
      try { \
          \$migrations = \$manager->migrate(\$time); \
          foreach (\$migrations as \$migration) { \
              echo \$migration->getName().' '.\$migration->getDescription().PHP_EOL; \
          } \
      } catch (Exception \$e) { \
          \$migration = \$manager->getMigrationById(array_pop(array_reverse(\$manager->getPendingMigrations()))); \
          \$result    = new IOError('Migration: '.\$migration->getName().' | Errorcode: '.\$e->getMessage()); \
          echo \$result->message; \
          return 1; \
      } \
      ";
    fi
  }

.marketplaceDevScripts: &marketplaceDevScripts |
  export FILENAME="${CI_COMMIT_REF_NAME//[\/\.]/-}";
  export EXTRACTION_PATH="$RUNNER_PATH/shop-$FILENAME";

  function downloadProcess() {
    RUNNER_PATH_ZIP="$EXTRACTION_PATH.zip";
    DOWNLOAD_URL="https://build.jtl-shop.de/get/shop-$FILENAME.zip";

    curl -o $RUNNER_PATH_ZIP $DOWNLOAD_URL
    unzip -qo $RUNNER_PATH_ZIP -d $EXTRACTION_PATH
    rm $RUNNER_PATH_ZIP
  }
  function deployProcess() {
    if [[ -d "$STAGING_MARKETPLACE_PATH/update/migrations/" ]]; then
      MIGRATION_COUNT_BEFORE=$(ls -1 $STAGING_MARKETPLACE_PATH/update/migrations/ | wc -l);

      sudo /usr/bin/rsync -rt -og --chown=nginx:nginx $EXTRACTION_PATH/. $STAGING_MARKETPLACE_PATH

      MIGRATION_COUNT_AFTER=$(ls -1 $STAGING_MARKETPLACE_PATH/update/migrations/ | wc -l);

      if [[ $MIGRATION_COUNT_AFTER -gt $MIGRATION_COUNT_BEFORE ]]; then
        php -r "
        require_once '$STAGING_MARKETPLACE_PATH/includes/globalinclude.php'; \
        \$time    = date('YmdHis'); \
        \$manager = new MigrationManager(Shop::Container()->getDB()); \
        try { \
            \$migrations = \$manager->migrate(\$time); \
            foreach (\$migrations as \$migration) { \
                echo \$migration->getName().' '.\$migration->getDescription().PHP_EOL; \
            } \
        } catch (Exception \$e) { \
            \$migration = \$manager->getMigrationById(array_pop(array_reverse(\$manager->getPendingMigrations()))); \
            \$result    = new IOError('Migration: '.\$migration->getName().' | Errorcode: '.\$e->getMessage()); \
            echo \$result->message; \
            return 1; \
        } \
        ";
      fi
    else
      sudo /usr/bin/rsync -rt -og --chown=nginx:nginx $EXTRACTION_PATH/. $STAGING_MARKETPLACE_PATH
    fi
  }

code_quality:
  stage: codeQuality
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - *codeQualityScripts
    - codeQualityCheck
  artifacts:
    paths: [code-quality-report.txt]
  only:
    - branches
  except:
    - master

removeContainerCodeQuality:
  stage: cleanUpCodeQuality
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    GIT_STRATEGY: none
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - *cleanUpTesting
    - failedCodeQuality
  only:
    - branches
  except:
    - master
    - /^release\/.*$/
  when: on_failure

test_PHP_7.2:
  stage: test
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    PHP_VERSION_TEST_STEP: '7.2'
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - *testingScript
    - testing
  only:
    - branches
  except:
    - master

test_PHP_7.3:
  stage: test
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    PHP_VERSION_TEST_STEP: '7.3'
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - *testingScript
    - testing
  only:
    - branches
  except:
    - master

removeContainerPHP7.2:
  stage: cleanUpTest
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    GIT_STRATEGY: none
    PHP_VERSION_TEST_STEP: '7.2'
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - *cleanUpTesting
    - failedTesting
  only:
    - branches
  except:
    - master
    - /^release\/.*$/
  when: on_failure

removeContainerPHP7.3:
  stage: cleanUpTest
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    GIT_STRATEGY: none
    PHP_VERSION_TEST_STEP: '7.3'
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - *cleanUpTesting
    - failedTesting
  only:
    - branches
  except:
    - master
    - /^release\/.*$/
  when: on_failure

build:
  stage: build
  before_script:
    - *preBuildDeployScripts
    - dockerLogin
  script:
    - buildProcess
  after_script:
    - *postBuildDeployScripts
    - dockerLogout
  only:
    - master
    - /^release\/.*$/
    - tags
  tags:
    - build

deploy:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  before_script:
    - *preBuildDeployScripts
    - dockerLogin
  script:
    - deployProcess
    - buildscript pipeline:shop5up "$CI_PROJECT_DIR" "$CI_COMMIT_REF_NAME" "$CI_COMMIT_SHA"
  after_script:
    - *postBuildDeployScripts
    - dockerLogout
  only:
    - master
    - /^release\/.*$/
    - tags
  when: on_success
  tags:
    - build

removeContainer:
  stage: cleanUp
  script:
    - *cleanUpScript
    - failedScript
  only:
    - master
    - /^release\/.*$/
    - tags
  when: on_failure
  tags:
    - build

tagsDeployment:
  stage: tagsDeployment
  variables:
    GIT_STRATEGY: none
  script:
    - buildscript release:deployment "$CI_BUILD_SERVER_ARCHIVE_PATH"
  except:
    - branches
  only:
    - tags
  tags:
    - build

stagingPreDeploy:
  stage: stagingPre
  variables:
    GIT_STRATEGY: none
  script:
    - *devCheckoutScripts
    - downloadProcess
  only:
    - master
  tags:
    - dev-checkout

stagingDevPhp72:
  stage: staging
  variables:
    GIT_STRATEGY: none
  script:
    - *devCheckoutScripts
    - deployProcess 72
  environment:
    name: staging master php 7.2
    url: https://$STAGING_USER-72-master.$STAGING_DOMAIN
  only:
    - master
  tags:
    - dev-checkout

stagingDevPhp73:
  stage: staging
  variables:
    GIT_STRATEGY: none
  script:
    - *devCheckoutScripts
    - deployProcess 73
  environment:
    name: staging master php 7.3
    url: https://$STAGING_USER-73-master.$STAGING_DOMAIN
  only:
    - master
  tags:
    - dev-checkout

stagingMarketplaceDev:
  stage: stagingMarketplaceDev
  variables:
    GIT_STRATEGY: none
  script:
    - *marketplaceDevScripts
    - downloadProcess
    - deployProcess
  environment:
    name: staging marketplace dev
    url: https://$STAGING_MARKETPLACE_DOMAIN
  only:
    - master
  tags:
    - test-marketplace